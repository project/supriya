To cutomize the Horizontal Menu
===============================

1. Review the last few lines in styles.css and update the image path/name based on your need. 
   There are sample images under images/Original Images directory.
2. Find the following lines in page.tpl.php and update the targets

<div id="mymenu">
	<ul>
		<li id="home"><a href="/"><span><em>Home</em></span></a></li>
		<li id="about"><a href="/announcements/about/"><span><em>About</em></span></a></li>
		<li id="contact"><a href="/announcements/contact/"><span><em>Contact</em></span></a></li>
		<li id="forums"><a href="/forums/"><span><em>Forums</em></span></a></li>
	</ul>
</div>
 
3.Save the files and reload the page after you delete cache and temporary internet files. 